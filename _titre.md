\begin{titlepage}
    \begin{center}

        
        \vspace*{2cm}
        
        \normalsize
        
        THE ASSEMBLY / L'ASSEMBLÉE
        
        \vspace{3cm}
          
        
       Étude:\\ 
        \vspace{.5cm}
        \huge
        Aquisition et latence \\
         \vspace{.5cm}
        \large
        en contexte vidéo scénique
        
        \vspace{.5cm}
        \normalsize
        Étape 1; évaluation Mac Pro 5.1
        \vspace{2cm}

        \normalsize
       	PRÉSENTÉ À:\\
        Production Porte Parole
	
		\vspace{1.5cm}

   		\normalsize
       	PAR:\\
       	Guillaume Arseneault
       	
       	\vfill
       	
       	\normalsize
       	\today
       	
        
         \vspace{3.0cm}


    \end{center}

\end{titlepage}


