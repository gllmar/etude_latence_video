# Chapitre 2

### Méthodologie suite 

téléchargement de la vidéo de timecode, voir scripts en référence
transcodage vers codec hap afin de maximiser l'efficience graphique. 
Le recours à un codec sans compression temporelle sert à éviter d'ajouter de l'ambiguité de décompression.
Utilisation du logiciel AVF.Batch.Converter.app de Vidvox pour faire la conversion.
Suppression de la piste audio; préserver la cadence à 60fps; 


### Utilisation de la carte blackmagic en entrée et en sortie

Question : 
* est-ce que la latence s'accumule plus on ajoute des entrées et des sorties
* est-ce qu'il est préférable d'utiliser une entrée et une sortie sur une même carte 
* est-ce qu'il est préférable d'utiliser une carte en entré et une carte en sortie dédié
* est-ce qu'une latence est associé à la conversion en sortie?
hypothèse: 



en sortie :

Latence cummulative, 1080p innutilisable


Millumin semble couper des frames (d'ou l'output réduit à 20 fps)
autour de 30 après la mise à jour
blacksyphon ajoute de la latence 



Cross semble plus consistant à 30 fps alors que non-cross dip à 20 fps assez souvent.

Si output = mauve
  aller dans les propriété de la carte de sortie  et sélectionner video is converted to rgb 4:4:4:4 au lieu de Y cb Cr 4:2:2




### Itération matériel 

depuis la première étude 


Ordinateur : 

```
Model Name:	MacBook Pro
  Model Identifier:	MacBookPro11,5
  Processor Name:	Intel Core i7
  Processor Speed:	2.5 GHz
  Number of Processors:	1
  Total Number of Cores:	4
  L2 Cache (per Core):	256 KB
  L3 Cache:	6 MB
  Memory:	16 GB
  Boot ROM Version:	MBP114.0172.B00
  SMC Version (system):	2.30f1
  Serial Number (system):	C17PRALAG8WP
  Hardware UUID:	714F5C03-33D5-55D3-B908-DF66EEB51322
```

carte Graphique:

```
AMD Radeon R9 M370X:

  Chipset Model:	AMD Radeon R9 M370X
  Type:	GPU
  Bus:	PCIe
  PCIe Lane Width:	x8
  VRAM (Total):	2048 MB
  Vendor:	ATI (0x1002)
  Device ID:	0x6821
  Revision ID:	0x0083
  ROM Revision:	113-C5670E-777
  gMux Version:	4.0.20 [3.2.8]
  EFI Driver Version:	01.00.777
```


Thunderbolt:
```
Echo Express SE II TB2:

  Vendor Name:	Sonnet Technologies, Inc.
  Device Name:	Echo Express SE II TB2
  Vendor ID:	0x8
  Device ID:	0xB
  Device Revision:	0x1
  UID:	0x0008000B0024A2E0
  Route String:	3
  Firmware Version:	21.1
  Port (Upstream):
  Status:	Device connected
  Link Status:	0x2
  Speed:	Up to 20 Gb/s x1
  Current Link Width:	0x2
```

Carte aquisition / diffusion:
```
2x Blackmagic Decklink Duo

serial1 : 2274940592
serial2 : 3310652640
```

### resultat test macbook

1080p quadXquad bk cross = 15 fps -> innutilisable
720p quadXquad bk cross = 30fps -> 4 frames -> 66.4 ms




### test comparatif avec Mac Pro

1080p quadXquad bk cross = 15 fps -> innutilisable
720p quadXquad bk cross = 30 fps -> 4 frames -> 66.4 ms


