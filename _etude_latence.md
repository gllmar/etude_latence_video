---
geometry: margin=2.5cm
papersize: letter
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
	\lhead{\textbf{Aquisition et latence}}
	\chead{}
	\rhead{\today}
	\lfoot{\MYhref{mailto:gllm@artificiel.org}{gllm@artificiel.org}}
	\cfoot{\thepage}
	\rfoot{\MYhref{https://gllmar.github.io}{gllmar.github.io}}
	\renewcommand{\headrulewidth}{.5pt}
	\renewcommand{\footrulewidth}{.5pt}
	\usepackage{svg}
	\usepackage[T1]{fontenc}
	\usepackage[french]{babel}
	\selectlanguage{french}
    \newcommand{\MYhref}[3][darkgray]{\href{#2}{\color{#1}{#3}}} 

    \usepackage{afterpage}

	\newcommand\blankpage{%
    	\null
    	\thispagestyle{empty}%
    	\addtocounter{page}{-1}%
    	\newpage}


---



\tableofcontents

\newpage

# Contexte

Dans le cadre de la pièce "The Assembly" 4 caméras seront utilisées pour transmettre le plus directement possible leurs signaux vers des télévisions situées au-dessus de la scène. Afin de pouvoir contrôler chacune de ces surfaces vidéo, il est envisagé qu'un ordinateur agisse comme intermédiaire entre les caméras et les écrans. 4 flux vidéo haute résolution composés d'un signal de caméra ainsi que d'autres éléments graphiques à déterminer seront composé dans un logiciel puis envoyer vers ces écrans vidéo.  

Le travail de l'ordinateur consistera à acquérir les flux des caméras vidéos, effectuer une composition temps réel (surimpression d'éléments graphiques, fondus au noir) et transmettre les signaux vers les 4 écrans.

Un enjeu que soulève l'usage d'un ordinateur dans cet environnement est l'introduction de latence entre l'acquisition et la diffusion d'un signal. 

Cette latence doit être minimisée étant donné que le spectateur est en position de voir l'action des comédiens captée par les caméras et sa diffusion. Plus la latence sera perceptible plus l'effet de direct sera perdu voir même agaçant. 


# Objectifs


* Déterminer avec une configuration matérielle donnée la latence entre l'acquisition d'une source captée par une caméra et sa diffusion. 


| CAPTATION  | → |  TRAITEMENT 			| → |DIFFUSION 	|
|:----------:|---|:--------------------:|---|-----------|
|			 |   | (latence en MS)	   	|   |			|	 
Table: Latence induite entre la captation et la diffusion d'un signal vidéo


* Évaluer si la configuration matérielle donnée est en mesure d'accomplir l'acquisition de 4 caméras et la transmission de 4 flux vidéo HD en préservant une latence minimum.

L'ordinateur en question est un Macpro 5.1 dont le détail de la configuration est en annexe.


# Cadre théorique 

La quantité de latence en millisecondes qu'une carte d'acquisition induit dans un circuit de transmission vidéo n'est pas une donnée qui est publiquement affichée par les manufacturiers de matériel vidéo. 
La plupart des produits affichent le terme *low-latency* sans donner davantage de détail. On semble s'entendre pour dure qu'une latence basse (Low latency) est un temps entre l'acquisition et la diffusion sous 100 millisecondes(Eberlein)[^Eberlein]. Une donnée importante ressortant du texte d'Eberlein est l'approche sérielle traitement du signal vidéo. Soit une image est traitée puis la prochaine, puis la prochaine. Le résultat d'un traitement sériel  est une fixité temporelle de la latence, cette temporalité est la somme du temps accumulé par chaque opération. En terme informatique on appelle ce principe de fonctionnement le *FIFO* (first in first out). Avant pouvoir à nouveau capter une image, l'image précédente doit avoir terminé son acquisition.    

Ainsi une image captée sortira nécessairement un nombre précis de millisecondes plus tard. 

| Temps 1  		| Temps 2  				| Temps 3   	| Temps 4   	| Temps 5   	|
|:----------:	|:--------------------:	|:---------:	|:---------:	|:---------:	|
| Captation 1 	|					   	|				|				|				|	 
|   		 	| Traitement  1			| 				| 				| 				|
|			 	| Captation	2			|Diffusion 1	|				|				|	
|				| 						|Traitement 2	|				|				|
|				|						|Captation 3	|Diffusion 2	|				|
|				|						|				|Traitement 3	|				|	
|				|						|				|				| Diffusion 3 	|		
Table: Sérialisation des opérations sur l'acquisition de 3 images vidéo en fonction d'un temps arbitraire

Une des raisons qui justifie l'usage du terme générique *low latency* est que la latence en milliseconde est grandement affectée par un ensemble de facteurs qui n'appartiennent pas exclusivement au matériel d'acquisition notamment[^Cast];

* la source du signal (caméra)
* la résolution du signal
* le type de signal de transport (SDI vs HDMI)
* la cadence (nombre d'images par secondes)
* la présence ou non de compression 
* la carte d'acquisition
* le système d'exploitation
* La qualité de l'implémentation du micrologiciel permettant l'acquisition (driver)
* Le logiciel effectuant l'acquisition
* Le traitement
* le moniteur (affichage)  

Le constat d'une revue de la littérature à ce sujet est que peu de documentation provenant des manufacturiers et encore moins d'études indépendantes expose en profondeur la quantité de latence ainsi qu'une méthodologie de tests. Encore plus rares sont les études portant sur une configuration multi entrée, d'où l'utilité d'étudier la question et de la documenter. 


[^Eberlein]:en ligne [https://www.vision-systems.com/content/dam/VSD/solutionsinvision/Resources/Sensoray_video-latency_article_FINAL.pdf](https://www.vision-systems.com/content/dam/VSD/solutionsinvision/Resources/Sensoray_video-latency_article_FINAL.pdf). L'auteur et le site ne donnent pas date de publication de cet article, mais étant donné qu'il se concentre sur une résolution standard, l'article doit dater d'avant 2010. Les principes généraux concernant la latence demeurent de vigueur sauf que dans notre cas nous nous concentrons sur une cadence progressive soit des images complètes au lieu de travailler avec des images entrelacées (champs pair et impair envoyés séparément).  
[^Cast]: liste inspirée par les lectures suivantes  : [http://www.cast-inc.com/blog/white-paper-understanding-and-reducing-latency-in-video-compression-systems](http://www.cast-inc.com/blog/white-paper-understanding-and-reducing-latency-in-video-compression-systems), [http://troikatronix.com/troikatronixforum/discussion/870/low-latency-capture-card](http://troikatronix.com/troikatronixforum/discussion/870/low-latency-capture-card), [https://www.datapath.co.uk/tbd/whitepapers/datapath_low_latency.pdf](https://www.datapath.co.uk/tbd/whitepapers/datapath_low_latency.pdf)

# Hypothèse 

Nous supposons que l'acquisition via le matériel d'acquisition sur la configuration soit une latence basse (*low latency*) c'est-à-dire autour de 100ms entre l'acquisition et la diffusion. Nous supposons que la latence s'accumulera en fonction du nombre d'entrées (sérialisation de la capture) mais qu'elle ne sera pas influencée par le nombre de sorties vidéo (parallélisation des opérations graphiques). 

\pagebreak


# Méthodologie

## Protocole d'expérimentation

L'objet de cette étude consiste à comparer la latence observée en fonction d'une configuration matérielle et logiciel précis. La méthode retenue pour calculer la latence est de comparer un vidéo source comportant des repères visuels à chaque image à son émission et à sa diffusion. 

Pour ce faire, une capture d'écran qui regroupe la vidéo de timecode (source) et cette même vidéo captée depuis le matériel et visible dans une fenêtre de monitorage permet de voir combien d'images sont latentes. La quantité de latence présente entre l'émission et la diffusion est la valeur que nous cherchons à systématiser.   

Les opérations de traitements vidéo sont les opérations entre l'émission et la diffusion d'une source:

| ÉMISSION  | → | CAPTATION  | → | CONVERSION | → | AQUISITION  | → |DIFFUSION 	|
|:---------:|---|:----------:|---|:--------:  |---|:-----------:|---|----------	|   
| Timecode  |   | Caméra     |   | HDMI→SDI   |   | Decklink    |   | Moniteur	|
Table: Traitement du signal entre émission et diffusion

## Spécifications de la source vidéo

La source vidéo[^source_video] utilisée a la caractérisque d'avoir des repères numéroté à chaque image. Elle est encodée dans un format exempt de compression temporelle afin de réduire au maximum à la fois le travail de l'ordinateur et la possibilité d'introduire des artéfacts de compression temporelle dans la capture d'écran.

[^source_video]:[Timecode with 60 fps Test https://www.youtube.com/watch?v=ZrQsl5AGAMo ](https://www.youtube.com/watch?v=ZrQsl5AGAMo)


| Composantes       | Spécifications           			|
| ------------- 	|-------------						|
| Résolution     	| 1920x1080							|
| Cadence     		| 60 images/secondes				|
| Codec     		| Prores 422						|
Table: Caractéristique vidéo de l'échantillon de timecode



## Taxonomie des tests 

Deux tests sont au menu, soit :

* Miroir de l'écran  
* Acquisition caméra

Dans le test miroir de l'écran, une image miroir de l'écran d'affichage de l'ordinateur est émise depuis une sortie sur la carte vidéo de l'ordinateur, est convertie en HD-SDI puis est envoyée vers la carte d'acquisition pour capture et visionnement. Ce test sert d'échantillon afin de trouver la latence la plus courte étant donné qu'il exclut des facteurs externes tels que la latence induite par la caméra ou par le moniteur. 

Dans le test impliquant la caméra; celle-ci filme le vidéo de timecode sur l'écran de l'ordinateur. Le signal de la caméra sort en HDMI, est convertie en SDI par la micro-converter puis acquis par la carte d'acquisition, une fenêtre de monitorage affiche le vidéo filmé depuis le logiciel testé. 

Dans les deux cas, une capture d'écran est prise incluant le vidéo source et la fenêtre de monitorage du logiciel testé. L'ensemble des tests sont effectués à une cadence de 59.97 fps leur résolution est 1080p (image progressive) à la fois en diffusion ainsi qu'en acquisition. 

## Cadence et résolution des tests.

| Résolution       	| Cadence           | Images 		|
| ------------- 	|-------------		| ---			|
| 1920 x 1080     	| 60 img/sec		| progressive 	|
Table:Spécifications du signal utilisé pour les tests

Nous effectuerons les tests à une résolution de 1920x1080 pixels, car c'est c'est la résolution native aux télévisions disponibles à bon marché en 2018. 

Les tests seront conduits à une cadence fixe de 60 images progressive par secondes pour les raisons suivantes. 

* C'est la cadence de rafraichissement native aux moniteurs de visionnement ainsi qu'aux cartes graphiques. 
* C'est la vitesse d'acquisition maximum des cartes d'acquisitions suggérées. 
* C'est la cadence maximum de la caméra testée tout en étant un standard assez répandu

Nous utilisons des images pleines au lieu d'entrelacées car l'entrelacement ajoute des artefacts en doublant le temps d'acquisition d'un cadre complet.

Étant donné que nous cherchons la latence minimale, nous voulons maximiser la vitesse d'acquisition afin de réduire au minimum le temps d'acquisition d'un cadre vidéo.


## Calcul de la latence en MS

Le calcul de latence est la différence de temps en nombre d'images entre la vidéo émise (fenêtre de lecture vidéo) puis celle acquise (fenêtre de monitorage du logiciel testé).
La valeur qui nous intéresse est le temps en millisecondes entre ces deux événements. Pour avoir cette valeur, il faut multiplier le nombre d'images de latence par le temps qu'une image en millisecondes est affichée à l'écran. 

Le calcul pour convertir le nombre d'images de latences en millisecondes est le suivant:


$$ latence(ms) = difference(images) \times {1000 \over cadence} $$ 

Puisque nos tests sont fait à 60 images par secondes nous pouvons simplifier comme suis.

$$ latence(ms) = difference(images) \times 16.\overline{6} $$

\pagebreak


## Branchements projetés 

Ce schéma rapporte le branchement vidéo projeté pour le spectacle.

Soit : 

* 4 caméras en direct, 
* 4 convertisseurs HDMI -> SDI
* L'ordinateur et ses composantes
* les sorties vers les écrans 

![Branchements vidéo\label{connections}](svg/branchements_macpro_projet.svg)

\pagebreak

# Observations

L'ensemble des opérations effectué sur l'ordinateur et notamment le détail de sa configuration sont en annexe.

## Nombre de sorties et résolution

Malgré la mise à jour entière du système, le système tel qu'évalué n'a pas réussi à supporter nativement 5 sorties digitales haute résolution (1920x1080) lors de la séance de test. 

Le maximum atteint de manière stable fut de 4 sorties digitales à la résolution 720p sans matériel externe.
Il a été souligné par Apple que cette configuration (double carte sur un Mac Pro pour obtenir 5 sorties digitales) nécessite un adaptateur actif [^duallink]

[^duallink]:Mac Pro (Early 2009), Mac Pro (Mid 2010), Mac Pro (Mid 2012): Issues with three displays and multiple DVI, HDMI connections https://support.apple.com/en-ca/HT203389 

## Système graphique instable

À certaines occasions, en branchant/débranchant des moniteurs afin d'obtenir la configuration graphique souhaitée, le système cesse complètement de fonctionner. Cet arrêt complet de la machine est un Kernel Panic dont un log est situé en annexe.

\pagebreak

## Branchements lors des tests de latences [^branchements]

Ce schéma rapporte le branchement des composantes lors des tests de latence.

Les différences par rapport aux branchements projetés sont les suivantes :

* Une caméra branchée dans la carte d'acquisition au lieu de 4
* Afin de simuler, le multiécran, les sorties de l'ordinateur sont branchés dans une matrice PIP qui permet un affichage de tous les signaux de manière simultanée.  


![Branchements vidéo\label{connections}](svg/branchements_macpro_tests.svg)

[^branchements]:à noter que lors des tests l'adaptateur `mini-DP->DVI Dual Link` n'était pas disponible. La solution fut d'avoir recours à des sorties virtuelles (Syphon) au lieu de sorties physiques. 

\pagebreak


# Résultats

## Media Express

L'usage de ce logiciel devrait démontrer minimum étant donné que ce logiciel est créé par la compagnie qui produit la carte d'acquisition. Par contre, il n'est pas possible d'utiliser ce logiciel dans le contexte du spectacle étant donné qu'il sert seulement à acquérir du flux afin de le convertir en vidéo (log and capture).

Deux tests ont été effectués, le premier est une boucle miroir de la sortie de l'ordinateur de manière à pouvoir isoler la latence de la caméra. Le deuxième est un test d'acquisition de la caméra.


| nombre d'entrées   | nombre de sorties  	| Latence (images) | Latence (ms)  			|
|-------------		 |-------------------	|------------------	|-------------------	|		
| 1					 |1		 				|5  				| 83.33					|
Table: Media Express miroir

![ Media Express miroir\label{connections}](img/media_express_miroir.png)


| nombre d'entrées   | nombre de sorties  	| Latence (images) | Latence (ms)  			|
|-------------		|-------------------	|------------------	|-------------------	|		
| 1					|1		 				|6  				| 99.96					|
Table: Media Express 1x1

![ Media Express latence 1x1\label{connections}](img/media_express_1x1.png)

\pagebreak
## Millumin

| nombre d'entrées   | nombre de sorties  	| Latence (images) | Latence (ms)  		|
|-------------		|-------------------	|------------------	|-------------------|		
| 1					|1		 				|6  				| 99.96				|
Table: latence millumin 1x1

![ latence millumin 1x1 \label{connections}](img/millumin_1x1.png)


| nombre d'entrées   | nombre de sorties  	| Latence (images) | Latence (ms)  	|
|-------------		|-------------------	|------------------	|-------------------	|		
| 1					|4		 				|8  				| 133.333				|
Table: latence millumin 1x4

![ latence millumin 1x4 \label{connections}](img/millumin_1x4.png)


| nombre d'entrées   | nombre de sorties  	| Latence (images) | Latence (ms)  	|
|-------------		|-------------------	|------------------	|-------------------	|		
| 4					|4		 				|10  				| 166.666				|
Table: latence millumin 4x4

![ latence millumin 4x4 \label{connections}](img/millumin_4x4.png)


\pagebreak

# Analyse des résultats

## Facteurs influençant la latence

### Caméra
Le test *Media Express miroir* montre une latence minimale totale de 5 images. Ce test omet volontairement la présence de la caméra qui est essentielle au contexte de travail afin d'isoler sa latence intrinsècte. Le *Media Express 1x1* montre une latence de 6 images ce qui semble pointer vers une latence d'une image attribuable à l'utilisation de la caméra. 

### Système d'exploitation

Afin de pouvoir isoler l'influence du système d'exploitation dans le cadre de cette étude, le test miroir a été reproduit sur un système Linux optimisé pour l'échantillonnage vidéo temps réel. 

|composantes 					| modèle			|
|-------------------			|---------------	|
|CPU 						 	| intel i7 7700k 	|
|GPU 							| nvidia 1080ti		|
|ram 							| 32g				|
|OS 							| archlinux			|
|kernel							| 4.16				|
|window manager					| i3wm				|
|acquisition 					|DeckLink Studio 4K |
Table: Spécifications matérielles du test Miroir sous Linux 

![ Media Express latence 1x1\label{connections}](img/linux_mirror_crop.png)


| Système 			  	| Latence (images) | Latence (ms)  			|
|-------------------	|------------------	|-------------------	|		
|macOS		 			|5  				| 83.33					|
|Linux		 			|4  				| 66.67					|
Table: Comparaison des tests miroir entre macOS et Linux

En comparant les deux tests et en tenant pour acquis qu'il n'y a pas de différence notable entre les deux modèles de carte d'acquisition, de carte graphique, d'architecture processeur, on note qu'une image de latence supplémentaire (16.667 ms) est attribuable à l'utilisation de macOS au lieu de Linux. 

### Conversion
Le micro-converter ne devrait pas ajouter de latence significative (moins d'une image par seconde) si on en suit la conversation suivante [^forum_3]

[^forum_3]: [https://forum.blackmagicdesign.com/viewtopic.php?f=4&t=3689](https://forum.blackmagicdesign.com/viewtopic.php?f=12&t=32908)
  

### Matériel d'acquisition
Selon un employé de Blackmagic, l'acquisition ne devrait pas rajouter plus 2 images de latence [^forum_2]

[^forum_2]: [https://forum.blackmagicdesign.com/viewtopic.php?f=12&t=32908](https://forum.blackmagicdesign.com/viewtopic.php?f=12&t=32908)


### Logiciel d'acquisition 

Le minimum de latence en matière capture caméra (6 images;99.96 millisecondes) a été atteint à la fois par le test *Media Express 1x1* ainsi que le test *millumin 1x1* ce qui montre une intégration adéquate du protocole de capture dans le logiciel Milumin. 

### Sommaire de la latence en fonction des étapes de traitement 

En compilant l'ensemble des résultats préalable, dans une situation de capture 1x1, la latence se distribue comme suis. 

| CAPTATION | → | CONVERSION | → | OS 		| → | AQUISITION  | → |DIFFUSION 	| TOTAL 	|
|:---------:|---|:----------:|---|:--------:|---|:-----------:|---|:---------:	|:---------:|
| caméra    |   | SDI        |   | macOS 	|   | BK SDK	  |   |		  		|			|
|    1      |   | < 1	     |   | 3 	    |   |  	2         |   |	< 1			| 6 images	|
|	16.6	|	|			 |	 | 49.8		|	|	33.2	  |	  |				| 99.96ms 	| 						
Table: Nombre d'images de latence en fonction de l'étape de traitement			


\pagebreak


### Latence minimum comparée à d'autres tests sous OSX


En utilisant le résultat du test *Media Express miroir* nous arrivons à une latence de 5 images. Cette latence minimum sur un système Macintosh en utilisant une recopie d'écran et un logiciel d'acquisition comme méthode de test est aussi corroboré par les échanges d'individus à ce sujet[^forum_1]. 


> A MacBook Pro with an Intensity Extreme capturing a mirrored display via The black magic software
> 
>> 3 frames latency 
>
> A simple QC composition with the video input patch -
> 
>> 6 frame latency 
>
> A Mac Pro with an Intensity Pro capturing a mirrored display via 
> A simple QC composition with the video input patch - 
>
>> 5 frames
>
> --<cite>James Sheridan, 2013 </cite> 

[^forum_1]: http://kineme.net/forum/DevelopingCompositions/BlackmagicIntensityVsMatroxMX02capturecardlatencies#comment-25759

À noter que sur la configuration graphique donnée, il n'a pas été possible d'atteindre 3 images de latence même en utilisant le logiciel fourni par Blackmagic. Il faut aussi spécifier que le vidéo test utilisé par James Sheridan semble avoir été produit à une cadence de 24 images par secondes (noté sur les noms des fichiers mis en exemple). 

![ tests Sheridan @24fps?\label{connections}](img/test_sheridan.png)

Il est donc possible que les résultats des tests soit discutables étant donné la vidéo de timecode semble être à 24p alors que l'écran est rafraichi à 60 Hz et que la carte d'acquisition doit nécessairement s'adapter à la cadence afin de fonctionner. Si cela est le cas, cela pourrait signifier une variation assez importante dans le temps en millisecondes que constitue la latence réelle. 

En utilisant la constante en MS calculé pour une image à une cadence de 24 images par secondes, on peut rapporter les résultats en MS des tests de Sheridan.

$$ 1000/24 = 41.\overline{6} $$


|tests acquisitions 			| frame@24p		| temps en ms|
|-------------------			|---------------|------------|
|miroir Blackmagic (MacBook)  	| 3   			| 124.98     |
|miroir QTZ (MacBook)			| 6				| 249.96	 |
|miroir QTZ (Mac Pro)			| 5				| 208.30	 |
Table: latence des tests Sheridan @24fps rapportée en MS 

En rapportant les résultats en millisecondes, nous nous approchons davantage de la latence observée dans notre étude. En comparant le test sur le MacBook et le Mac Pro, nous pouvons estimée que si un test miroir avait été produit par Sheridan sur le Mac Pro, sa latence calculée aurait été entre 2 et 3 images à 24 fps soit d'entre 83.2 et 125ms. La différence de latence entre le MacBook et le Mac Pro est attribuable à l'usage du Thunderbolt (MacBook) qui semble ajouter une image de latence vs le PCIe(Mac Pro) .  


## Variation de la latence

La latence de notre configuration matérielle varie en fonction du nombre d'entrées tel que supposé par contre, elle varie aussi proportionnellement au nombre de sorties sollicitées. 


| nombre d'entrées   | nombre de sorties  	| Latence (images) | Latence (ms)  		|
|-------------		|-------------------	|------------------	|-------------------|
| 1					|1		 				|6  				| 99.96				|
| 1					|4		 				|8  				| 133.333			|		
| 4					|4		 				|10  				| 166.666			|
Table: Latence en fonction de la charge graphique dans millumin


Cette latence proportionnelle à la charge graphique est aussi accompagnée de perte de fluidité observable, voire même de perte d'images entière. Par exemple dans la capture d'image associée au test Millumin 4x4, on peut voir sur l'image de la caméra le chiffre 14 et le 12 dus à la persistance de l'exposition de la caméra vidéo en mode automatique sur fond noir. Par contre le chiffre 13 est complètement absent. Cela signifie que le système entier est accaparé par la charge graphique et a manqué de traiter l'image. 

De plus, pendant le test millumin 4x4 certaines fonctions du système tardent à répondre, notamment l'interface usagée, la lecture vidéo et les logiciels utilisés. 

## Limitation du matériel

La configuration matérielle donnée montre que dans l'usage projeté il semble y avoir une limitation (*bottle neck*) qui rend l'usage de ce matériel non recommandable. Au moment où l'on demande à l'ordinateur de produire le nombre de sorties et d'entrée projeté (4x4), l'ordinateur devient instable et la latence excède le seuil de latence basse. 

\pagebreak

# Bilan des limites

## Échantillon restreint

L'expérimentation se concentre uniquement sur un fournisseur avec une configuration matérielle, il est possible que dans un contexte de test similaire d'autres fournisseurs de matériel arrivent à des résultats différents. D'autant plus, aucun test n'été fait sur la plateforme Windows qui bénéficie souvent d'un support matériel/graphique avancé.     

## Résolutions

Afin d'élucider certaines ambiguïtés concernant les limites du matériel, les tests devraient être reproduits à une résolution de 720p/60 fps au lieu de 1080p/60fps. Si un comportement diffère de celui observé. 

## 59.97 vs 60

Il est possible que lors des tests certains dispositifs notamment la caméra émettent du signal en 59.97 et que plusieurs tests soient effectués avec cette cadence et que cela a introduit de légères variations dans les résultats.

## Système graphique vieillissant 

Le type de carte graphique présent dans le MacPro bien que supporté nativement par le système d'exploitation pourrait potentiellement bénéficier d'un rafraichissement matériel. Il est possible que dans notre test 4x4 nous taquinions la limite de traitement de la carte. Afin d'éliminer cette donnée, il faudrait reproduire les tests avec une carte graphique récente également supportée. 

## Deux cartes graphiques

Il est possible que le recours à deux cartes induise de l'instabilité. Les tests devraient être reproduits en enlevant une carte graphique afin de s'assurer que la latence n'est pas influencée par la présence de deux cartes graphiques. 


\pagebreak

# Conclusion

La latence minimale calculée dans l'acquisition d'une caméra jusqu'à sa diffusion correspond à une latence de 99.96 millisecondes qui se qualifie comme étant une faible latence au seuil de la perception visuelle. 

Par contre, la configuration donnée n'est pas apte à être utilisée pour réaliser l'acquisition et la diffusion de 4 flux vidéo HD de manière simultanée. L'instabilité générale du système et les images manquantes constituent des facteurs inflexibles (show stopper) d'autant plus que la latence calculée dans le cas d'usage dépasse amplement les 100ms. Cette étude démontre qu'il existe actuellement une correspondance proportionnelle entre la latence et la charge graphique qui semble affecter plus que confortablement les performances de la machine.  

## Recommandations

Il faudrait continuer les expérimentations en prenants comptes du bilan des limites. Il faudrait donc reproduire les tests à une résolution plus basse (720p) et procéder à une autre série de tests avec une seule carte graphique. De cette manière, il serait plus limpide de circonscrire la limite révélée par l'ensemble des expérimentations accomplie dans le cadre de cette étude. 

Si les tests à résolution plus basse révèlent une normalisation de la latence indépendante du nombre de sorties vidéo, cela pourrait signifier qu'une limitation graphique est atteinte dans l'étude actuelle en utilisant une haute résolution. Dans ce cas, l'investissement dans une carte graphique récente pourrait potentiellement permettre d'obtenir le résultat escompté sur la machine fournie.

Si les tests à une seule carte démontrent davantage de stabilité, une solution devrait alors être envisagée pour résoudre un problème inhérent au nombre de sorties prévues. Sur le marché actuellement des solutions disposant de 5 sorties supportées par macOS sont très rares. Les cartes qui ont 5 sorties physiques ont généralement 4 sorties actives et un port qui est doublé sur deux ports différents par exemple: gtx1080 ou rx580. 

Dans le cas où le recours à une carte graphique récente ne change rien à la latence, il faudrait réévaluer le choix du système Macpro 5.1 pour effectuer ce travail. D'autre approche ou système d'exploitation sont  


<!-- 
tl;DR
Dans la configuration actuelle du mac pro 4 in et 4+1 out n'est pas possible. 
Plus on ajoute des entrés et des sorties plus on ajoute de la latence.
À une entrée et une sortie, on est autour de la limite du perceptible (100 ms) ce qui est potable.
Par contre à 4 entrées et 4 sorties, l'ordinateur ne tient pas la route, il manque des images et la latence est plus que perceptible. 
Il est possible qu'en réduisant la résolution on réduise la latence (1080p -> 720p)
Il est possible que la configuration à deux cartes induise de l'instabilité
Il est possible qu'avec une carte graphique plus récente on contourne la limitation de la latence proportionnelle à la charge graphique.

Pour avoir 5 sorties vidéo actuellement ça va quand même prendre 2 cartes graphiques.

La suite: 
Retester avec une carte graphique
Retester l'ensemble  à 720p
étudier le marché actuel des cartes graphiques,
choisir une(deux) carte graphique compatible avec le projet  
réévaluer la récupération du Mac Pro 5.1 pour l'usage projet
 -->


\pagebreak

# Annexes

## Configuration matérielle

### Ordinateur[^config]

| Composantes       | Modèle           					| Spécification  |
| ------------- 	|-------------						|-------------------|
| CPU     		 	| Quad-Core Intel Xeon				|2.8 GHz 			|
| GPUs      		| 2xATI Radeon HD 5770      		|1 gb Vram			|
| OS 				| 10.7								| -> 10.13			|
Table: MacPro 5.1

 [^config]:Le détail des manipulations effectuées sur l'ordinateur avant les tests est en annexe

### Captation
| Composantes       | Modèle           					| Spécification  |
| ------------- 	|-------------						|-------------------|
| Sony     		 	| HDR-CX405							|1080p, 59.97 fps 		|
Table: Caméra

### Aquisition 		
| Composantes       | Modèle           					| Spécification  |
| ------------- 	|-------------						|-------------------|
| Blackmagic 		|DeckLink Duo 2       				|4x SDI IN / OUT 	|
Table: Carte PCI acquisition

### Conversion
| Composantes       | Modèle           					| Spécification  |
| ------------- 	|-------------						|-------------------|
| Blackmagic 		|Micro Converter       				|HDMI->SDI 			|
Table: Conversion SDI

### Logiciels
| Composantes       | Version           				| Spécification  |
| ------------- 	|-------------						|-------------------|
| Millumin			|2.18.q				       			|Utilisation de la source native		|
| Black Magic Media Express | 3.5.5						| afin de tester l'implémentation logicielle la plus proche des développeurs |
Table: Logiciels

\pagebreak

## Opérations sur le MacPro

### Mise à jour du MacPro du microcode de la carte mère
* Mise à jour microcode matériel de la carte mère du Macpro 
	* nécessaire sinon aucune mise à jour vers un système d'exploitation récent n'est possible.
* Mise à niveau du système d'exploitation de l'ordinateur 
	* Une première tentative de passer directement de 10.7 à 10.13 ne fut fructueuse, car 10.13 requiert minimalement 10.8 pour procéder. Par contre, 10.8 n'est pas disponible en mise à jours actuellement depuis 10.7
	* La méthode fructueuse fut de mettre à jour vers un système intermédiaire soit 10.10, faire la mise à jour du microcode, puis  mettre à jour le système vers 10.13 à travers le app store.

### Installation d'un SSD 
Afin de préserver le projet Seed fonctionnel dans l'état de réception de la machine, un système d'exploitation récent, indépendant de la configuration de SEED fut installé sur un disque SSD. Cela à pour effet secondaire de rendre beaucoup plus rapide notamment au démarrage ainsi qu'au lancement des applications. 

### Changement de carte d'acquisition
le MacPro est doté de 5 fentes d'expansion dont 4 sont utilisées par les cartes graphiques. Le projets Seed l'avait équipé d'une carte d'acquisition de type *Blackmagic Intensity Pro* (deux entrées HDMI) qui fut remplacé par une  carte *Decklink Duo 2* (4 entrée SDI) 




\pagebreak
## Kernel Panic
### message d'erreur
```
Anonymous UUID:       98F3876B-FF31-7F06-7359-2EC110A146FF

Sun May 20 08:44:38 2018

*** Panic Report ***
panic(cpu 4 caller 0xffffff7f8b3abd7a): 
"bool IOAccelDisplayMachine2::display_mode_did_change(uint32_t): 
vendor driver returns false"
@/BuildRoot/Library/Caches/com.apple.xbs/Sources/IOAcceleratorFamily_kexts/
IOAcceleratorFamily-378.18.1/Kext2/IOAccelDisplayMachine.cpp:490
Backtrace (CPU 4), Frame : Return Address
0xffffff9115b83490 : 0xffffff800826e166 
0xffffff9115b834e0 : 0xffffff8008396714 
0xffffff9115b83520 : 0xffffff8008388a00 
0xffffff9115b835a0 : 0xffffff8008220180 
0xffffff9115b835c0 : 0xffffff800826dbdc 
0xffffff9115b836f0 : 0xffffff800826d99c 
0xffffff9115b83750 : 0xffffff7f8b3abd7a 
0xffffff9115b83780 : 0xffffff7f8b3d87d5 
0xffffff9115b837c0 : 0xffffff7f894aca46 
0xffffff9115b83870 : 0xffffff7f89499a10 
0xffffff9115b838b0 : 0xffffff7f894a3f15 
0xffffff9115b838d0 : 0xffffff7f8949ead0 
0xffffff9115b83940 : 0xffffff7f894aa5e3 
0xffffff9115b83960 : 0xffffff7f894a620b 
0xffffff9115b839b0 : 0xffffff7f89499f83 
0xffffff9115b83aa0 : 0xffffff7f894a5f22 
0xffffff9115b83ad0 : 0xffffff80088c2218 
0xffffff9115b83b20 : 0xffffff7f894b0f2b 
0xffffff9115b83b70 : 0xffffff80088cae07 
0xffffff9115b83cb0 : 0xffffff8008344a84 
0xffffff9115b83dc0 : 0xffffff8008273680 
0xffffff9115b83e10 : 0xffffff80082509dd 
0xffffff9115b83e60 : 0xffffff80082634eb 
0xffffff9115b83ef0 : 0xffffff80083735ad 
0xffffff9115b83fa0 : 0xffffff8008220986 
      Kernel Extensions in backtrace:
         com.apple.iokit.IOGraphicsFamily(519.15)
         [D5F2A20D-CAB0-33B2-91B9-E8755DFC34CB]@0xffffff7f89490000->0xffffff7f894d8fff
            dependency: com.apple.iokit.IOPCIFamily(2.9)
            [580E0204-B37A-32BA-AC27-E8DBCA519B1F]@0xffffff7f88a94000
         com.apple.iokit.IOAcceleratorFamily2(378.18.1)
         [BAA0383C-9650-3934-B04A-69008F757A2C]@0xffffff7f8b3a1000->0xffffff7f8b437fff
   


```
\pagebreak

### Description matériel
```
EOF
Model: MacPro5,1, BootROM MP51.0085.B00, 4 processors, 
Quad-Core Intel Xeon, 2.8 GHz, 8 GB, SMC 1.39f11
Graphics: ATI Radeon HD 5770, ATI Radeon HD 5770, PCIe
Graphics: ATI Radeon HD 5770, ATI Radeon HD 5770, PCIe
Memory Module: DIMM 1, 4 GB, DDR3 ECC, 1066 MHz, 0x0198, 
0x393936353532352D3031322E4130304C4620
Memory Module: DIMM 2, 4 GB, DDR3 ECC, 1066 MHz, 0x0198, 
0x393936353532352D3031322E4130304C4620
AirPort: spairport_wireless_card_type_airport_extreme (0x14E4, 0x8E), 
Broadcom BCM43xx 1.0 (5.106.98.102.30)
Bluetooth: Version 6.0.5f3, 3 services, 27 devices, 1 incoming serial ports
Network Service: Ethernet 1, Ethernet, en0
PCI Card: ATI Radeon HD 5770, Display Controller, Slot-1
PCI Card: ATI Radeon HD 5770, Display Controller, Slot-2
PCI Card: DeckLink Duo 2, Video, Slot-4
Serial ATA Device: HL-DT-ST DVD-RW GH61N
Serial ATA Device: ST31000524AS, 1 TB
Serial ATA Device: KINGSTON SA400S37240G, 240.06 GB
USB Device: USB Bus
USB Device: USB Bus
USB Device: BRCM2046 Hub
USB Device: Bluetooth USB Host Controller
USB Device: USB Bus
USB Device: USB Bus
USB Device: USB Bus
USB Device: USB Bus
USB Device: USB 2.0 Bus
USB Device: USB 2.0 Bus
USB Device: USB2.0 Hub
USB Device: ELECOM TrackBall Mouse
USB Device: USB Keyboard
FireWire Device: built-in_hub, Up to 800 Mb/sec
Thunderbolt Bus: 
```