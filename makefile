SHELL := /bin/bash
PY=python
PANDOC=pandoc

BASEDIR=$(CURDIR)

OUTPUTDIR=$(ROOTDIR)/output

SOURCEFILE=_etude_latence.md
TITLE_PAGE=_titre.md
SOURCENAME=etude_latence

.PHONY: all pdf

pdf:
	@echo 'making a pdf'
	@echo "$(SOURCEFILE)"
	@echo "$(SOURCENAME)"
	ROOTDIR=`dirname $(BASEDIR)`
	
	pandoc "$(BASEDIR)"/$(TITLE_PAGE) "$(BASEDIR)"/$(SOURCEFILE)  -o "$(BASEDIR)/$(SOURCENAME).pdf" 

